﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OOP2.Views
{
    using OOP2.ViewModels;
    using OOP2.Classes;
    /// <summary>
    /// Логика взаимодействия для UserForm.xaml
    /// </summary>
    public partial class UserForm : UserControl
    {
        public UserForm()
        {
            InitializeComponent();  
        }

        public static DependencyProperty FormTypeProperty
            = DependencyProperty.Register("FormType", typeof(Classes.RegionTypes), typeof(UserForm));

        public Classes.RegionTypes FormType
        {
            get { return (Classes.RegionTypes)GetValue(FormTypeProperty); }
            set { SetValue(FormTypeProperty, value); }
        }
        public string RegionName
        {
            get { return getValue(cpRegionName); }
            private set { setValue(cpRegionName, value); }
        }
        public string AdministrativeCenter
        {
            get { return getValue(cpAdministrativeCenter); }
            private set { setValue(cpAdministrativeCenter, value); }
        }
        public string Governer
        {
            get { return getValue(cpGoverner); }
            private set { setValue(cpGoverner, value); }
        }
        public string Anthem
        {
            get { return getValue(cpAnthem); }
            private set { setValue(cpAnthem, value); }
        }
        public string RegCode
        {
            get { return getValue(cpRegCode); }
            private set { setValue(cpRegCode, value); }
        }
        public string Area
        {
            get { return getValue(cpArea); }
            private set { setValue(cpArea, value); }
        }
        public string Population
        {
            get { return getValue(cpPopulation); }
            private set { setValue(cpPopulation, value); }
        }
        public IEnumerable<Classes.AutonomousOkrug> SelectedOkrugs
        {
            get
            {
                return from AutonomousOkrug item in lbxNestedRegions.SelectedItems select item;
            }
        }

        private string getValue(ContentPresenter cp)
        {
            string res = (cp.ContentTemplate.FindName("Answer", cp) as TextBox).Text;
            return res;
        }
        private void setValue(ContentPresenter cp, string value)
        {
            (cp.ContentTemplate.FindName("Answer", cp) as TextBox).Text = value;
        }

        public void MarkFields(FieldStyles fst)
        {
                      cpRegionName.Tag = (fst & FieldStyles.RegionName)           != 0 ? "InvalidImportant" : "ValidImportant";
            cpAdministrativeCenter.Tag = (fst & FieldStyles.AdministrativeCenter) != 0 ? "InvalidImportant" : "ValidImportant";
                        cpGoverner.Tag = (fst & FieldStyles.Governer)             != 0 ? "Invalid" : "Valid";
                          cpAnthem.Tag = (fst & FieldStyles.Anthem)               != 0 ? "Invalid" : "Valid";
                         cpRegCode.Tag = (fst & FieldStyles.RegCode)              != 0 ? "Invalid" : "Valid";
                            cpArea.Tag = (fst & FieldStyles.Area)                 != 0 ? "Invalid" : "Valid";
                      cpPopulation.Tag = (fst & FieldStyles.Population)           != 0 ? "Invalid" : "Valid";
        }
        public void ClearForm()
        {
            RegionName = String.Empty;
            AdministrativeCenter = String.Empty;
            Governer = String.Empty;
            Anthem = String.Empty;
            RegCode = String.Empty;
            Area = String.Empty;
            Population = String.Empty;
            lbxNestedRegions.UnselectAll();
        }
    }
}
