﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OOP2.ViewModels
{
    using OOP2.Views;
    using OOP2.Classes;

    internal static class ViewModel
    {
        public static ObservableCollection<AutonomousOkrug> AOIndepended = new ObservableCollection<AutonomousOkrug>();

        public static bool MakeRegion(UserForm uf)
        {
            FieldStyles fs = FieldStyles.None;
            int area = 0, population = 0, regCode = 0;

            if (string.IsNullOrWhiteSpace(uf.RegionName))
                fs |= FieldStyles.RegionName;
            if (string.IsNullOrWhiteSpace(uf.AdministrativeCenter))
                fs |= FieldStyles.AdministrativeCenter;
            if (!string.IsNullOrWhiteSpace(uf.RegCode) && !int.TryParse(uf.RegCode, out regCode))
                fs |= FieldStyles.RegCode;
            if (!string.IsNullOrWhiteSpace(uf.Area) && !int.TryParse(uf.Area, out area))
                fs |= FieldStyles.Area;
            if (!string.IsNullOrWhiteSpace(uf.Population) && !int.TryParse(uf.Population, out population))
                fs |= FieldStyles.Population;


            uf.MarkFields(fs);

            if (fs != FieldStyles.None)
                return false;

            Region rg = null;
            if (uf.FormType == RegionTypes.Krai)
            {
                rg = new Krai(uf.RegionName, uf.AdministrativeCenter);
                foreach (AutonomousOkrug okrug in uf.SelectedOkrugs.ToArray())
                {
                    (rg as Krai).AddOkrug(okrug);
                    AOIndepended.Remove(okrug);
                }
            }
            if (uf.FormType == RegionTypes.Oblast)
            {
                rg = new Oblast(uf.RegionName, uf.AdministrativeCenter);
                foreach(AutonomousOkrug okrug in uf.SelectedOkrugs.ToArray())
                {
                    (rg as Oblast).AddOkrug(okrug);
                    AOIndepended.Remove(okrug);
                }
            }
            if (uf.FormType == RegionTypes.AutonomousOkrug)
            {
                rg = new AutonomousOkrug(uf.RegionName, uf.AdministrativeCenter);
                AOIndepended.Add(rg as AutonomousOkrug);
            }

            rg.Governer = uf.Governer;
            rg.Anthem = uf.Anthem;
            rg.RegCode = regCode;
            rg.Area = area;
            rg.Population = area;

            return true;
        }
    }
}
