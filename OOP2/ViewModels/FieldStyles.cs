﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2.ViewModels
{
    public enum FieldStyles
    {
        None = 0,
        RegionName = 1,
        AdministrativeCenter = 2,
        Neccessary = 3,
        Governer = 4,
        Anthem = 8,
        RegCode = 16,
        Area = 32,
        Population = 64
    }
}
