﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace OOP2.Classes
{
    public class Krai : Region
    {
        private List<AutonomousOkrug> _okrugs = new List<AutonomousOkrug>();
        public ReadOnlyCollection<AutonomousOkrug> Okrugs { get; }

        public string KraiName { get; set; }

        public Krai() 
            : base()
        {
            Okrugs = new ReadOnlyCollection<AutonomousOkrug>(_okrugs);
        }

        public Krai(string name, string center) 
            : this()
        {
            AdministrativeCenter = center;
            KraiName = name;
        }

        public override string GetInfo()
        {
            string form = "Край: {0}, " + base.GetInfo();
            return String.Format(form, string.IsNullOrWhiteSpace(KraiName) ? "-" : KraiName);
        }
        public virtual void AddOkrug(AutonomousOkrug okrug)
        {
            if (okrug != null && !_okrugs.Contains(okrug))
                _okrugs.Add(okrug);
        }
    }
}