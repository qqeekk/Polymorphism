﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2.Classes
{
    /// <remarks>
    /// Конституция РФ:
    /// Статья 5.4: "Во взаимоотношениях с федеральными органами государственной власти все субъекты Российской Федерации между собой равноправны."
    /// Статья 66.4: "Отношения автономных округов, входящих в состав края или области, могут регулироваться федеральным законом и договором между органами государственной власти автономного округа и, соответственно, органами государственной власти края или области."
    /// </remarks>
    public class Oblast : Region
    {
        private List<AutonomousOkrug> _okrugs = new List<AutonomousOkrug>();
        public ReadOnlyCollection<AutonomousOkrug> Okrugs { get; }

        public string OblastName { get; set; }

        public Oblast()
            : base()
        {
            Okrugs = new ReadOnlyCollection<AutonomousOkrug>(_okrugs);
        }

        public Oblast(string name, string center)
            : this()
        {
            OblastName = name;
            AdministrativeCenter = center;
        }

        public override string GetInfo()
        {
            string form = "Область: {0}, " + base.GetInfo();
            return String.Format(form, string.IsNullOrWhiteSpace(OblastName) ? "-" : OblastName);
        }

        public virtual void AddOkrug(AutonomousOkrug okrug)
        {
            if (okrug != null && !_okrugs.Contains(okrug))
                _okrugs.Add(okrug);
        }
    }
}
