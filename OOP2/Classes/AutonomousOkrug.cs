﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2.Classes
{
    public class AutonomousOkrug : Region
    {
        public AutonomousOkrug(string name, string center) 
            : this()
        {
            OkrugName = name;
            AdministrativeCenter = center;
        }

        public AutonomousOkrug()
            : base() { }

        public string OkrugName { get; set; }

        public override string GetInfo()
        {
            string form = "Автономный округ: {0}, " + base.GetInfo();
            return String.Format(form, string.IsNullOrWhiteSpace(OkrugName) ? "-" : OkrugName);
        }
    }
}
