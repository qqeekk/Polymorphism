﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP2.Classes
{
    public abstract class Region
    {
        private static List<Region> _regions = new List<Region>();
        public static ReadOnlyCollection<Region> Regions { get; } = new ReadOnlyCollection<Region>(_regions);

        public static string GetAllInfo()
        {
            string res = null;
            int N = Regions.Count;

            for(int i = 0; i < N; i++)
            {
                res += String.Format("{0}) {1}{2}", i+1, Regions[i].GetInfo(), i+1 == N ? "." : ";\n");
            }
            return res;
        }
        public string AdministrativeCenter { get; set; }
        public string Governer { get; set; }
        public string Anthem { get; set; }
        public int RegCode { get; set; }
        public int Area { get; set; }
        public int Population { get; set; }

        public Region()
        {
            _regions.Add(this);
        }
        public virtual string GetInfo()
        {
            string form = "Код: {0}, Административный центр: {1}, Губернатор: {2}";
            return String.Format(form, RegCode == 0 ? "отсутствует" : RegCode.ToString(), 
                                       string.IsNullOrWhiteSpace(AdministrativeCenter) ? "-" : AdministrativeCenter,
                                       string.IsNullOrWhiteSpace(Governer) ? "-" : Governer);
        }
    }
}
